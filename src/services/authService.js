import jwtDecode from "jwt-decode";
import http from "./httpService";
import { apiEndpoint, tokenName } from "../config.json";

const apiUrl = `${apiEndpoint}/auth`;

http.setJwt(getJwt());

export async function login(email, password) {
  const { data: jwt } = await http.post(apiUrl, { email, password });
  localStorage.setItem(tokenName, jwt);
}

export function loginWithJwt(jwt) {
  localStorage.setItem(tokenName, jwt);
}

export function logout() {
  localStorage.removeItem(tokenName);
}

export function getCurrentUser() {
  try {
    const jwt = localStorage.getItem(tokenName);
    return jwtDecode(jwt);
  } catch (ex) {
    return null;
  }
}

export function getJwt() {
  return localStorage.getItem(tokenName);
}

export default {
  login,
  logout,
  getCurrentUser,
  getJwt,
  loginWithJwt
};
