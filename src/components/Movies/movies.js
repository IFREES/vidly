import React, { Component } from "react";
import _ from "lodash";
import Pagination from "../common/pagination";
import ListGroup from "../common/listGroup";
import MoviesTable from "./moviesTable";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import { getMovies, deleteMovie } from "../../services/movieService";
import { paginate } from "../../utils/paginate";
import { getGenres } from "../../services/genreService";
import SearchBox from "../common/searchBox";

export default class movies extends Component {
  state = {
    movies: [],
    genres: [],
    sortColom: { path: "title", order: "asc" },
    searchQuery: "",
    selectedGenre: null,
    pageSize: 4,
    currentPage: 1
  };

  async componentDidMount() {
    const { data: g } = await getGenres();
    const genres = [{ name: "All Genres", _id: "All Genres" }, ...g];
    const { data: movies } = await getMovies();
    this.setState({ genres, movies });
  }

  handleDeleteMovie = async movie => {
    const originalMovies = this.state.movies;
    const movies = originalMovies.filter(m => m._id !== movie._id);
    this.setState({ movies });

    try {
      await deleteMovie(movie._id);
    } catch (ex) {
      if (ex.response && ex.response.status === 404) {
        toast.error("This movie is already deleted");
      }
      this.setState({ movies: originalMovies });
    }
  };

  handleLikeToggle = movie => {
    const movies = [...this.state.movies];
    const index = movies.indexOf(movie);
    movies[index].liked = !movies[index].liked;
    this.setState({ movies });
  };

  handlePageChange = page => {
    this.setState({ currentPage: page });
  };

  handleGenreSelect = genre => {
    this.setState({ selectedGenre: genre, searchQuery: "", currentPage: 1 });
  };

  handleSearch = query => {
    this.setState({ searchQuery: query, selectedGenre: null, currentPage: 1 });
  };

  handleSort = sortColom => {
    this.setState({ sortColom });
  };

  getPagedData = () => {
    const {
      pageSize,
      currentPage,
      sortColom,
      selectedGenre,
      movies: allMovies,
      searchQuery
    } = this.state;

    let filtered = allMovies;
    if (searchQuery)
      filtered = allMovies.filter(
        m => m.title.toLowerCase().search(searchQuery.toLowerCase()) !== -1
      );
    else if (selectedGenre && selectedGenre.name !== "All Genres")
      filtered = allMovies.filter(m => m.genre._id === selectedGenre._id);

    const sorted = _.orderBy(filtered, [sortColom.path], [sortColom.order]);

    const movies = paginate(sorted, currentPage, pageSize);
    return { totalCount: filtered.length, data: movies };
  };

  render() {
    const { length: count } = this.state.movies;
    const { genres, selectedGenre, pageSize, currentPage, sortColom, searchQuery } = this.state;
    const { user } = this.props;

    if (count === 0) return <p>Es sind Keine Filme in der Datenbank</p>;
    const { totalCount, data: movies } = this.getPagedData();
    return (
      <div className="row">
        <div className="col-3">
          <ListGroup
            selectedItem={selectedGenre}
            items={genres}
            onItemSelect={this.handleGenreSelect}
          />
        </div>
        <div className="col">
          {user && (
            <Link to="/movies/new" className="btn btn-primary" style={{ marginBottom: 20 }}>
              NewMovie
            </Link>
          )}
          <SearchBox value={searchQuery} onChange={this.handleSearch} />
          <p>Es werden {totalCount} filme angezeigt</p>
          <MoviesTable
            movies={movies}
            sortColom={sortColom}
            onSort={this.handleSort}
            onLikeToggle={this.handleLikeToggle}
            onDelete={this.handleDeleteMovie}
          />
          <Pagination
            itemsCount={totalCount}
            pageSize={pageSize}
            currentPage={currentPage}
            onPageChange={this.handlePageChange}
          />
        </div>
      </div>
    );
  }
}
