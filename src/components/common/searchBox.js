import React from "react";

const SearchBox = ({ value, onChange }) => {
  return (
    <input
      value={value}
      placeholder="Search..."
      onChange={e => onChange(e.currentTarget.value)}
      name="query"
      className="form-control my-3"
      type="text"
    />
  );
};

export default SearchBox;
